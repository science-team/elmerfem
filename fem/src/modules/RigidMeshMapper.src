!/*****************************************************************************/
! *
! *  Elmer, A Finite Element Software for Multiphysical Problems
! *
! *  Copyright 1st April 1995 - , CSC - IT Center for Science Ltd., Finland
! * 
! *  This program is free software; you can redistribute it and/or
! *  modify it under the terms of the GNU General Public License
! *  as published by the Free Software Foundation; either version 2
! *  of the License, or (at your option) any later version.
! * 
! *  This program is distributed in the hope that it will be useful,
! *  but WITHOUT ANY WARRANTY; without even the implied warranty of
! *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! *  GNU General Public License for more details.
! *
! *  You should have received a copy of the GNU General Public License
! *  along with this program (in file fem/GPL-2); if not, write to the 
! *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, 
! *  Boston, MA 02110-1301, USA.
! *
! *****************************************************************************/
!
!/******************************************************************************
! *
! *  Authors: Peter R�back
! *  Email:   Peter.Raback@csc.fi
! *  Web:     http://www.csc.fi/elmer
! *  Address: CSC - IT Center for Science Ltd.
! *           Keilaranta 14
! *           02101 Espoo, Finland 
! *
! *  Original Date: 24.2.2009
! *
! *****************************************************************************/


!------------------------------------------------------------------------------
!>  Subroutine for mapping the mesh using analytical commands of scaling, 
!> rotation, translation and smoothing. Additionally may include a grading field in [0,1]
!> that may be solved from a Laplace equation. Provides often the most economical way
!> of distorting the mesh.
!> \ingroup Solvers
!------------------------------------------------------------------------------
SUBROUTINE RigidMeshMapper( Model,Solver,dt,Transient )
!------------------------------------------------------------------------------
  USE CoordinateSystems
  USE ElementUtils
  USE ElementDescription
  USE ParallelUtils
  USE Types
  USE Lists
  USE DefUtils

  
  IMPLICIT NONE
!------------------------------------------------------------------------------
  TYPE(Model_t)  :: Model
  TYPE(Solver_t), TARGET :: Solver
  LOGICAL ::  Transient
  REAL(KIND=dp) :: dt
!------------------------------------------------------------------------------
!    Local variables
!------------------------------------------------------------------------------
  TYPE(ValueList_t),POINTER :: SolverParams, BC
  INTEGER :: i,j,k,n,m,t,dim,elem,bf_id,prev_bf_id,istat
  INTEGER :: NonlinIter, MaxNonlinIter
  INTEGER, POINTER :: NodeIndexes(:), RelaxPerm(:), VeloPerm(:)
  REAL(KIND=dp), POINTER :: PArray(:,:) => NULL()
  REAL(KIND=dp) :: x0(4), x1(4), RotMatrix(4,4),TrsMatrix(4,4),SclMatrix(4,4), &
      TrfMatrix(4,4),Identity(4,4), Origin(4),Angles(3),alpha, dCoord, Norm, dx(3)
  REAL(KIND=dp) :: at0,at1,at2,CPUTime,RealTime,Coeff
  REAL(KIND=dp), POINTER :: Xorig(:),Yorig(:),Zorig(:),Xnew(:),Ynew(:),Znew(:),&
      Relax(:), RelaxField(:),MeshDisplace(:,:),VeloVal(:)
  REAL(KIND=dp), ALLOCATABLE :: STIFF(:,:), FORCE(:)
  TYPE(Variable_t), POINTER :: VeloVar, RelaxVar
  TYPE(Mesh_t), POINTER :: Mesh
  LOGICAL :: Found,GotMatrix,GotRotate,GotTranslate,GotScale,Visited=.FALSE.,&
      UseOriginalMesh, Cumulative, GotDisplace, GotRelaxField=.FALSE., &
      CalculateVelocity
  LOGICAL, POINTER :: NodeDone(:)
  TYPE(Element_t), POINTER :: Element
  TYPE(Nodes_t), SAVE :: Nodes
  TYPE(ValueList_t),POINTER :: BodyForce

  
  SAVE Xorig,Yorig,Zorig,Parray,MeshDisplace,Visited
   
  CALL Info( 'RigidMeshMapper','---------------------------------------',Level=4 )
  CALL Info( 'RigidMeshMapper','Performing analytic mesh mapping ',Level=4 )
  CALL Info( 'RigidMeshMapper','---------------------------------------',Level=4 )

  SolverParams => GetSolverParams()
  Mesh => Solver % Mesh

  Cumulative = GetLogical( SolverParams,'Cumulative Displacements',Found)
  UseOriginalMesh = .NOT. Cumulative

  dim = CoordinateSystemDimension()
  
  Xnew => Mesh % Nodes % x
  Ynew => Mesh % Nodes % y
  Znew => Mesh % Nodes % z
  n = SIZE( Xnew )
  m = Mesh % MaxElementNodes

  ALLOCATE( NodeDone(n), Relax(m), MeshDisplace(3,m) )
  NodeDone = .FALSE.

  CalculateVelocity = GetLogical( SolverParams,'Calculate Mesh Velocity',Found)
  IF( CalculateVelocity ) THEN
    VeloVar => VariableGet( Mesh % Variables,'Mesh Velo')
    IF(ASSOCIATED(VeloVar)) THEN
      VeloVal => VeloVar % Values
      VeloPerm => VeloVar % Perm
    ELSE
      n = SIZE( Xnew )
      ALLOCATE( VeloVal(dim*n), VeloPerm(n) )
      VeloVal = 0.0_dp
      DO i=1,n
        VeloPerm(i) = i
      END DO
      IF( dim == 2 ) THEN
        CALL VariableAddVector( Mesh % variables, Mesh, Solver,'Mesh Velo[Mesh Velo:2]',&
            dim, VeloVal, VeloPerm ) 
      ELSE
        CALL VariableAddVector( Mesh % variables, Mesh, Solver,'Mesh Velo[Mesh Velo:3]',&
            dim, VeloVal, VeloPerm ) 
      END IF
    END IF

    CALL InvalidateVariable( CurrentModel % Meshes, Solver % Mesh,'Mesh Velo' )	
  END IF



  ! If using original mesh as a reference mesh it must be saved,
  ! otherwise the analytic mapping does not require two meshes
  !------------------------------------------------------------
  IF( UseOriginalMesh ) THEN
    IF( .NOT. Visited ) THEN
      WRITE(Message,* ) 'Allocating new nodes of size: ',n
      CALL Info('RigidMeshMapper',Message,Level=6)
      ALLOCATE(Xorig(n),Yorig(n),Zorig(n))      
      Xorig = Xnew
      Yorig = Ynew
      Zorig = Znew
    END IF
  ELSE
    Xorig => Xnew
    Yorig => Ynew
    Zorig => Znew
  END IF


  IF( .NOT. Visited .AND. ASSOCIATED(Solver % Matrix) ) THEN
    N = Solver % Mesh % MaxElementNodes 
    ALLOCATE( FORCE(N), STIFF(N,N), STAT=istat )

    ! Implement moving and fixed BCs
    ! ------------------------------
    DO i=1,Model % NumberOFBCs
      BC => Model % BCs(i) % Values
      IF ( GetLogical(  BC, 'Moving Boundary', Found ) ) THEN
        CALL ListAddConstReal( BC,Solver % Variable % Name, 1.0_dp )
      ELSE IF ( GetLogical(  BC, 'Fixed Boundary', Found ) ) THEN
        CALL ListAddConstReal( BC,Solver % Variable % Name, 0.0_dp )
      END IF
    END DO

    CALL Info('RigidMeshMapper','Solving mesh relaxation field using Laplace',Level=6)
    
    MaxNonlinIter = GetInteger( SolverParams,&
       'Nonlinear System Max Iterations',Found)
    IF(.NOT. Found) MaxNonlinIter = 1
    
    Coeff = GetCReal( SolverParams,'Nonlinear Conductivity Coeffient',Found)

    DO NonlinIter = 1, MaxNonlinIter
      CALL DefaultInitialize()
      
      DO t=1, GetNOFActive()
        Element => GetActiveElement(t)
        n = GetElementNOFNodes()
        CALL LocalMatrix(  STIFF, FORCE, Element, n )
        CALL DefaultUpdateEquations( STIFF, FORCE )
      END DO
      
      CALL DefaultFinishAssembly()
      CALL DefaultDirichletBCs()
      Norm = DefaultSolve()      
      
      IF( Solver % Variable % NonlinConverged == 1 ) EXIT
    END DO

    DEALLOCATE( FORCE, STIFF )
  END IF

  RelaxVar => Solver % Variable
  IF( ASSOCIATED( RelaxVar ) ) THEN
    IF( ASSOCIATED( RelaxVar % Values ) ) THEN
      IF( SIZE( RelaxVar % Values ) > 0 ) THEN
        GotRelaxField = .TRUE.
        RelaxField => Solver % Variable % Values
        RelaxPerm => Solver % Variable % Perm
      END IF
    END IF
  END IF

    
  ! Initialize the mapping matrices
  Identity = 0.0d0
  DO i=1,4
    Identity(i,i) = 1.0d0
  END DO
  
  prev_bf_id = -1
  GotRotate = .FALSE.
  GotTranslate = .FALSE.
  GotScale = .FALSE.
  GotMatrix = .FALSE.
  
  at0 = CPUTime()

  
  DO elem = 1,Solver % Mesh % NumberOfBulkElements      
    Element => Solver % Mesh % Elements(elem)
    CurrentModel % CurrentElement => Element
    
    NodeIndexes => Element % NodeIndexes
    Model % CurrentElement => Element
    CALL GetElementNodes( Nodes )
    n  = GetElementNOFNodes()
    
    bf_id = ListGetInteger( Model % Bodies(Element % BodyId) % Values,'Body Force',Found )
    IF(.NOT. Found) CYCLE
    
    IF( ALL ( NodeDone(NodeIndexes(1:n)) ) ) CYCLE
    BodyForce => Model % BodyForces(bf_id) % Values
    

    IF( bf_id /= prev_bf_id ) THEN
      
      prev_bf_id = bf_id
      NULLIFY(Parray)
      
      ! Generic transformation matrix
      !--------------------------------
      Parray => ListGetConstRealArray( BodyForce,'Mesh Matrix', GotMatrix )
      IF ( GotMatrix ) THEN
        DO i=1,SIZE(Parray,1)
          DO j=1,SIZE(Parray,2)
            TrfMatrix(i,j) = Parray(j,i)
          END DO
        END DO
      END IF
      
      IF(.NOT. GotMatrix ) THEN
        TrsMatrix = Identity
        RotMatrix = Identity
        SclMatrix = Identity
        Angles = 0.0_dp
        
        ! Rotations around main axis:
        !---------------------------
        
        Parray => ListGetConstRealArray( BodyForce,'Mesh Rotate', GotRotate )      
        
        IF ( GotRotate ) THEN
          DO i=1,SIZE(Parray,1)
            Angles(1) = Parray(i,1) 
          END DO
        ELSE 
          Angles(1) = ListGetCReal( BodyForce,'Mesh Rotate 1', Found )
          IF( Found ) GotRotate = .TRUE.
          Angles(2) = ListGetCReal( BodyForce,'Mesh Rotate 2', Found )
          IF( Found ) GotRotate = .TRUE.
          Angles(3) = ListGetCReal( BodyForce,'Mesh Rotate 3', Found )
          IF( Found ) GotRotate = .TRUE.
        END IF
        
        IF( GotRotate ) THEN
          DO i=1,3
            Alpha = Angles(i) * PI / 180.0_dp
            
            IF( ABS(Alpha) < TINY(Alpha) ) CYCLE
            TrfMatrix = Identity
            
            SELECT CASE(i)
            CASE(1)
              TrfMatrix(2,2) =  COS(Alpha)
              TrfMatrix(2,3) = -SIN(Alpha)
              TrfMatrix(3,2) =  SIN(Alpha)
              TrfMatrix(3,3) =  COS(Alpha)
            CASE(2)
              TrfMatrix(1,1) =  COS(Alpha)
              TrfMatrix(1,3) = -SIN(Alpha)
              TrfMatrix(3,1) =  SIN(Alpha)
              TrfMatrix(3,3) =  COS(Alpha)
            CASE(3)
              TrfMatrix(1,1) =  COS(Alpha)
              TrfMatrix(1,2) = -SIN(Alpha)
              TrfMatrix(2,1) =  SIN(Alpha)
              TrfMatrix(2,2) =  COS(Alpha)
            END SELECT
            
            RotMatrix = MATMUL( RotMatrix, TrfMatrix )
          END DO
        END IF
        
        ! Translations:
        !---------------
        Parray => ListGetConstRealArray( BodyForce,'Mesh Translate', GotTranslate )
        IF ( GotTranslate ) THEN
          DO i=1,SIZE(Parray,1)
            TrsMatrix(i,4) = Parray(i,1)
          END DO
        ELSE 
          dCoord = ListGetCReal( BodyForce,'Mesh Translate 1', Found) 
          IF( Found ) THEN
            TrsMatrix(1,4) = dCoord
            GotTranslate = .TRUE.
          END IF
          dCoord = ListGetCReal( BodyForce,'Mesh Translate 2', Found) 
          IF( Found ) THEN
            TrsMatrix(2,4) = dCoord
            GotTranslate = .TRUE.
          END IF
          dCoord = ListGetCReal( BodyForce,'Mesh Translate 3', Found) 
          IF( Found ) THEN
            TrsMatrix(3,4) = dCoord
            GotTranslate = .TRUE.
          END IF
        END IF        

        ! Scaling:
        !---------
        Parray => ListGetConstRealArray( BodyForce,'Mesh Scale', GotScale )
        IF ( GotScale ) THEN
          DO i=1,SIZE(Parray,1)
            SclMatrix(i,i) = Parray(i,1)
          END DO
        ELSE 
          dCoord = ListGetCReal( BodyForce,'Mesh Scale 1', Found) 
          IF( Found ) THEN
            SclMatrix(1,1) = dCoord
            GotScale = .TRUE.
          END IF
          dCoord = ListGetCReal( BodyForce,'Mesh Scale 2', Found) 
          IF( Found ) THEN
            SclMatrix(2,2) = dCoord
            GotScale = .TRUE.
          END IF
          dCoord = ListGetCReal( BodyForce,'Mesh Scale 3', Found) 
          IF( Found ) THEN
            SclMatrix(3,3) = dCoord
            GotScale = .TRUE.
          END IF
        END IF 

        GotMatrix = GotRotate .OR. GotTranslate .OR. GotScale
        IF(GotMatrix) THEN
          ! Origin:
          !---------
          Origin = 0.0_dp
          Parray => ListGetConstRealArray( BodyForce,'Mesh Origin', Found )
          IF ( Found ) THEN
            DO i=1,SIZE(Parray,1)
              Origin(i) = Parray(i,1)
            END DO
          ELSE
            Origin(1) = ListGetCReal( BodyForce,'Mesh Origin 1', Found) 
            Origin(2) = ListGetCReal( BodyForce,'Mesh Origin 1', Found) 
            Origin(3) = ListGetCReal( BodyForce,'Mesh Origin 1', Found) 
          END IF
          
          TrfMatrix = MATMUL( TrsMatrix, RotMatrix )
          IF( GotScale ) THEN
            TrsMatrix = TrfMatrix
            TrfMatrix = MATMUL( SClMatrix, TrsMatrix )
          END IF
        END IF
      END IF
    END IF

    IF( .NOT. GotMatrix ) THEN
      MeshDisplace(1,1:n) = GetReal( BodyForce,'Mesh Displacement 1',GotDisplace)
      MeshDisplace(2,1:n) = GetReal( BodyForce,'Mesh Displacement 2',Found)
      GotDisplace = GotDisplace .OR. Found
      MeshDisplace(3,1:n) = GetReal( BodyForce,'Mesh Displacement 3',Found)
      GotDisplace = GotDisplace .OR. Found
    END IF

    IF(.NOT. (GotMatrix .OR. GotDisplace)) CYCLE

    ! Find the relaxation parameters that may interpolate the displacement between 
    ! moving and fixed walls.
    !------------------------------------------------------------------------------
    Relax(1:n) = GetReal( BodyForce,'Mesh Relax',Found)
    IF(.NOT. Found) THEN
      IF( GotRelaxField ) THEN
        IF( ALL ( RelaxPerm( NodeIndexes(1:n) ) /= 0 ) ) THEN
          Relax(1:n) = RelaxField( RelaxPerm( NodeIndexes(1:n) ) )
          Found = .TRUE.
        END IF
      END IF
    END IF
    IF(.NOT. Found ) Relax(1:n) = 1.0_dp    

    DO i=1,n
      j = NodeIndexes(i)
      IF(NodeDone(j)) CYCLE
      
      IF( GotMatrix ) THEN        
        x0(1) = Xorig(j)
        x0(2) = Yorig(j)
        x0(3) = Zorig(j)
        x0(4) = 1.0_dp
        
        x1 = MATMUL( TrfMatrix, x0 - Origin ) + Origin
        dx(1:3) = x1(1:3) / x1(4) - x0(1:3)
      ELSE
        dx(1:3) = MeshDisplace(1:3,i)
      END IF

      dx = Relax(i) * dx
      IF( CalculateVelocity ) THEN
        k = j
        IF( ASSOCIATED( VeloPerm) ) k = VeloPerm(j)
        IF( k > 0 ) THEN  
	  IF( dim == 2 ) THEN
	    VeloVal(2*k-1) = ( Xorig(j) + dx(1) - Xnew(j) ) / dt
	    VeloVal(2*k) = ( Yorig(j) + dx(2) - Ynew(j) ) / dt	
          ELSE
	    VeloVal(3*k-2) = ( Xorig(j) + dx(1) - Xnew(j) ) / dt
	    VeloVal(3*k-1) = ( Yorig(j) + dx(2) - Ynew(j) ) / dt	
	    VeloVal(3*k) = ( Zorig(j) + dx(3) - Znew(j) ) / dt		
          END IF
        END IF
      END IF

      Xnew(j) = dx(1) + Xorig(j)
      Ynew(j) = dx(2) + Yorig(j)
      Znew(j) = dx(3) + Zorig(j)

      NodeDone(j) = .TRUE.
    END DO

  END DO


  IF(.NOT. Visited ) THEN
    WRITE(Message,* ) 'Number of nodes mapped: ',COUNT( NodeDone )
    CALL Info('RigidMeshMapper',Message)
    at1 = CPUTime()
    IF( at1-at0 > 0.1_dp ) THEN
      WRITE(Message,* ) 'Coordinate mapping time: ',at1-at0
      CALL Info('RigidMeshMapper',Message)
    END IF  
    CALL Info('RigidMeshMapper','All done' ) 
  END IF

  DEALLOCATE( Relax, MeshDisplace, NodeDone )
  
  Visited = .TRUE.


CONTAINS


!------------------------------------------------------------------------------
  SUBROUTINE LocalMatrix(  STIFF, FORCE, Element, n )
!------------------------------------------------------------------------------
    REAL(KIND=dp) :: STIFF(:,:), FORCE(:)
    INTEGER :: n
    TYPE(Element_t), POINTER :: Element
!------------------------------------------------------------------------------
    REAL(KIND=dp) :: Basis(n),dBasisdx(n,3),DetJ,Grad(3),Cond,LocalRelax(n)
    LOGICAL :: Stat
    INTEGER :: i,j,t
    TYPE(GaussIntegrationPoints_t) :: IP
    TYPE(Nodes_t) :: Nodes
    SAVE Nodes
!------------------------------------------------------------------------------
    CALL GetElementNodes( Nodes )
    STIFF = 0.0d0
    FORCE = 0.0d0
    
    CALL GetScalarLocalSolution( LocalRelax )

    !Numerical integration:
    !----------------------
    IP = GaussPoints( Element )
    DO t=1,IP % n
      ! Basis function values & derivatives at the integration point:
      !--------------------------------------------------------------
      stat = ElementInfo( Element, Nodes, IP % U(t), IP % V(t), &
          IP % W(t),  detJ, Basis, dBasisdx )
      DO i=1,3
        Grad(i) = SUM( dBasisdx(:,i) * LocalRelax(1:n) )
      END DO
      Cond = 1.0_dp + Coeff * SQRT( SUM( Grad * Grad ) )

      ! Laplace operator
      !------------------
      STIFF(1:n,1:n) = Cond * STIFF(1:n,1:n) + IP % s(t) * DetJ * &
          MATMUL( dBasisdx, TRANSPOSE( dBasisdx ) )
    END DO
!------------------------------------------------------------------------------
  END SUBROUTINE LocalMatrix
!------------------------------------------------------------------------------

!------------------------------------------------------------------------------
END SUBROUTINE RigidMeshMapper
!------------------------------------------------------------------------------
